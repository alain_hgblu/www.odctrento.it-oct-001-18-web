<?php
# Metodo per attivazioni gestione contenuti CUSTOM
function create_post_type()
{

    # BANNERS
    register_post_type('banners',
        array(
            'labels' => array(
                'name' => __('Banners'),
                'singular_name' => __('Banners')
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'thumbnail', 'page-attributes')
        )
    );

    # APPUNTAMENTI
    register_post_type('Appuntamenti',
        array(
            'labels' => array(
                'name' => __('Appuntamenti'),
                'singular_name' => __('Appuntamenti')
            ),
            'hierarchical' => true,
            'rewrite' => array('slug' => 'appuntamenti', 'with_front' => false),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'excerpt')
        )
    );

    # AMMMINISTRAZIONE TRASPARENTE
    register_post_type('ammtrasparente',
        array(
            'labels' => array(
                'name' => __('Amministrazione trasparente'),
                'singular_name' => __('Amministrazione trasparente')
            ),
            'hierarchical' => true,
            'rewrite' => array('slug' => 'amministrazione-trasparente', 'with_front' => false),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
        )
    );

    # ISCRITTI
    register_post_type('alboiscritti',
        array(
            'labels' => array(
                'name' => __('Albo iscritti'),
                'singular_name' => __('Albo iscritti'),
                'search_items' => __('Ricerca iscritto')
            ),
            'rewrite' => array('slug' => 'albo-iscritti', 'with_front' => false),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'thumbnail', 'page-attributes')
        )
    );
}

add_action('init', 'create_post_type');

# Attiva il supporto delle thumbs per pagina, articoli, consiglio (custom), amministrazione_trasparente (custom)
add_theme_support('post-thumbnails', array('post', 'page', 'banners', 'consiglio', 'ammtrasparente', 'alboiscritti'));


/**
 * Widget Areas
 */
function areas_widgets_init()
{
    register_sidebar(
        array(
            'name' => 'Footer - Colonna 1',
            'id' => 'footer-1',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );

    register_sidebar(
        array(
            'name' => 'Footer - Colonna 2',
            'id' => 'footer-2',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );

    register_sidebar(
        array(
            'name' => 'Footer - Colonna 3',
            'id' => 'footer-3',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => ''
        )
    );

    register_sidebar(
        array(
            'name' => 'HP - Sportello cittadino - Intro',
            'id' => 'hp-sportello-0',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h2 class="color-white">',
            'after_title' => '</h2>'
        )
    );

    register_sidebar(
        array(
            'name' => 'HP - Sportello cittadino - colonna 1',
            'id' => 'hp-sportello-1',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3 class="color-white">',
            'after_title' => '</h3>'
        )
    );

    register_sidebar(
        array(
            'name' => 'HP - Sportello cittadino - colonna 2',
            'id' => 'hp-sportello-2',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3 class="color-white">',
            'after_title' => '</h3>'
        )
    );

    register_sidebar(
        array(
            'name' => 'HP - Sportello cittadino - colonna 3',
            'id' => 'hp-sportello-3',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3 class="color-white">',
            'after_title' => '</h3>'
        )
    );

    register_sidebar(
        array(
            'name' => 'HP - Sportello cittadino - colonna 4',
            'id' => 'hp-sportello-4',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3 class="color-white">',
            'after_title' => '</h3>'
        )
    );
}

# Attiva i widgets personalizzati
add_action('widgets_init', 'areas_widgets_init');


# Rimuove dati relativi alla sicurezza di WP (generator, versione, etc.)
remove_action('wp_head', 'wp_generator');
function wpbeginner_remove_version()
{
    return '';
}

add_filter('the_generator', 'wpbeginner_remove_version');

# Rimuove i commenti dal menu
function df_disable_comments_admin_menu()
{
    remove_menu_page('edit-comments.php');
}

add_action('admin_menu', 'df_disable_comments_admin_menu');

# Abilita gestione menu
function bootstrapstarter_wp_setup()
{
    add_theme_support('title-tag');
    add_theme_support('menus');
}

add_action('after_setup_theme', 'bootstrapstarter_wp_setup');

# Funzione per breadcrumbs
function the_breadcrumb()
{
    echo '<ol class="breadcrumb pb-0 mt-3 mb-0 px-0 bg-white">';
    if (!is_home()) {
        echo '<li class="breadcrumb-item 1"><a href="';
        echo get_option('home');
        echo '" title="">';
        echo 'Home';
        echo "</a></li>";
        if (get_post_type() == 'ammtrasparente') {
            echo '<li class="breadcrumb-item"><a href="/amministrazione-trasparente/" title="">Amministrazione trasparente</a></li>';
        }
        if (get_post_type() == 'page') {
            $sezione = '';
            $id_anchestor = end(get_post_ancestors());
            switch ($id_anchestor) {
                case 92: // ordine
                    $sezione = 'Ordine';
                    break;
                case 115: // albo
                    $sezione = 'Albo';
                    break;
                case 463: // tirocinio
                    $sezione = 'Tirocinio';
                    break;
                case 135: // formazione
                    $sezione = 'Formazione';
                    break;
                case 100: // modulistica
                    $sezione = 'Modulistica';
                    break;
                case 484: // normativa
                    $sezione = 'Normativa';
                    break;
            }

            if($sezione != '') {
                echo '<li class="breadcrumb-item">' . $sezione . '</li>';
            }
        }
        if(get_the_ID() == 484) {
            $sezione = 'Normativa';
            echo '<li class="breadcrumb-item">' . $sezione . '</li>';
        }

        if (get_post_type() == 'post' || get_post_type() == 'appuntamenti') {
            echo '<li class="breadcrumb-item">Comunicazioni</li>';
            if (get_post_type() == 'appuntamenti') {
                echo '<li class="breadcrumb-item"><a href="/appuntamenti/" title="Prossimi appuntamenti">Prossimi appuntamenti</a></li>';
            }
        }
        if (is_category() || is_single()) {
            echo '<li class="breadcrumb-item 2">';
            the_category(' </li><li class="breadcrumb-item 3"> ');
            /*if (is_single()) {
                echo '</li><li class="breadcrumb-item 4">';
                the_title();
                echo '</li>';
            }*/
        } /*elseif (is_page()) {
            echo '<li class="breadcrumb-item 5">';
            echo the_title();
            echo '</li>';
        }*/
    } elseif (is_tag()) {
        single_tag_title();
    } elseif (is_day()) {
        echo "<li>Archive for ";
        the_time('F jS, Y');
        echo '</li>';
    } elseif (is_month()) {
        echo "<li>Archive for ";
        the_time('F, Y');
        echo '</li>';
    } elseif (is_year()) {
        echo "<li>Archive for ";
        the_time('Y');
        echo '</li>';
    } elseif (is_author()) {
        echo "<li>Author Archive";
        echo '</li>';
    } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
        echo "<li>Blog Archives";
        echo '</li>';
    } elseif (is_search()) {
        echo "<li>Search Results";
        echo '</li>';
    }
    echo '</ol>';
}


# APPUNTAMENTI
function set_custom_appuntamenti_columns($columns)
{
    $columns = array(
        'title' => __('Title'),
        'data_evento' => __('Data evento', 'data evento'),
        'author' => __('Author')
    );

    return $columns;
}

// Add the data to the custom columns for the book post type:
function custom_appuntamenti_column($column, $post_id)
{
    switch ($column) {

        case 'data_evento' :
            $data_evento = get_post_meta($post_id, 'data_evento', true);
            echo date('d/m/Y', strtotime($data_evento));
            break;

    }
}

/*function prefix_parse_filter($query)
{
    global $pagenow;
    $current_page = isset($_GET['post_type']) ? $_GET['post_type'] : '';

    if ('appuntamenti' == $current_page && 'edit.php' == $pagenow) {

        if (!isset($_GET['archive'])) {
            $query->set('orderby', 'data_evento');
            $query->set('order', 'ASC');
            $query->set('meta_query', array(
                array(
                    'key' => 'data_evento',
                    'value' => date("Y-m-d"),
                    'compare' => '>=',
                    'type' => 'DATE'
                )
            ));
        } else {
            $query->set('orderby', 'data_evento');
            $query->set('order', 'DESC');
            $query->set('meta_query', array(
                array(
                    'key' => 'data_evento',
                    'value' => date("Y-m-d"),
                    'compare' => '<',
                    'type' => 'DATE'
                )
            ));
        }
    }
}

function add_submenu_appuntamenti()
{
    global $submenu;
    $permalink = admin_url('edit.php') . '?post_type=appuntamenti&archive';
    $submenu['edit.php?post_type=appuntamenti'][] = array('Archivio', 'manage_options', $permalink);
}

add_filter('manage_realestate_posts_columns', 'reorder_appuntamenti_columns');
add_filter('parse_query', 'prefix_parse_filter');
add_filter('manage_appuntamenti_posts_columns', 'set_custom_appuntamenti_columns');
add_action('manage_appuntamenti_posts_custom_column', 'custom_appuntamenti_column', 0, 2);
add_action('admin_menu', 'add_submenu_appuntamenti');*/


function admin_appuntamenti_filter($query)
{
    global $pagenow, $wp_post_types;

    if (is_admin() && $pagenow == 'edit.php') {

        if(isset($_GET['ADMIN_FILTER_APPUNTAMENTI']) && $_GET['ADMIN_FILTER_APPUNTAMENTI'] == 'archive') {
            $query->set('orderby', 'data_evento');
            $query->set('order', 'ASC');
            $query->set('meta_query', array(
                array(
                    'key' => 'data_evento',
                    'value' => date("Y-m-d"),
                    'compare' => '<',
                    'type' => 'DATE'
                )
            ));

            $labels = &$wp_post_types['appuntamenti']->labels;
            $labels->name = 'Archivio appuntamenti';

        } else {
            $query->set('orderby', 'data_evento');
            $query->set('order', 'DESC');
            $query->set('meta_query', array(
                array(
                    'key' => 'data_evento',
                    'value' => date("Y-m-d"),
                    'compare' => '>=',
                    'type' => 'DATE'
                )
            ));
        }
    }
}


function ba_admin_posts_filter_restrict_manage_posts()
{
    $current = isset($_GET['ADMIN_FILTER_APPUNTAMENTI']) ? $_GET['ADMIN_FILTER_APPUNTAMENTI'] : '';
?>
    <select name="ADMIN_FILTER_APPUNTAMENTI">
        <option value="">Prossimi appuntamenti</option>
        <option value="archive"<?php if($current == 'archive') echo ' selected' ?>>Archivio</option>
    </select>
<?php }
if(isset($_GET['post_type']) && $_GET['post_type'] == 'appuntamenti') {
    add_filter('parse_query', 'admin_appuntamenti_filter');
    add_action('restrict_manage_posts', 'ba_admin_posts_filter_restrict_manage_posts');
}

echo $query;
