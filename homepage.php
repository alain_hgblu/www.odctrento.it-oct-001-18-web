<?php
/**
 * Template Name: Homepage
 */
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ordine dei Commercialisti e degli Esperti Contabili di Trento e Rovereto</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet"
          href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/css/style.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/css/main.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body class="home">

<?php get_template_part('template-parts/header') ?>

<main class="cd-main-content">

    <div class="container-fluid sliderHP color-default mt-5">
        <div class="container">
            <h1 class="my-0">Ordine dei <br><strong class="color-corporate">Dottori Commercialisti</strong> <br>e degli <strong class="color-corporate">Esperti
                    Contabili</strong> <br>di <strong class="color-corporate">Trento</strong> e <strong class="color-corporate">Rovereto</strong></h1>
        </div>
    </div>

    <div class="container-fluid bg-corporate color-default m-0 p-0">
        <div class="container">
            <div class="row py-5">
                <div class="col-12 col-sm-6 newsHP">
                    <?php get_template_part('template-parts/news/news', 'hp'); ?>
                </div>
                <div class="col col-sm-6 appuntamentiHP">
                    <?php get_template_part('template-parts/eventi/eventi', 'hp'); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid bg-white color-default m-0 p-0">
        <div class="container">
            <div class="row row-eq-height py-5">
                <div class="col-12 col-sm-4 mb-3 py-4 text-center">
                    <div class="py-5 h-100 box-hp box-hp-albo">
                        <h2>Elenco iscritti</h2>
                        <p>Ricerca all’interno dell'albo</p>
                        <a href="/albo-iscritti/" title="Ricerca all’interno dell’albo iscritti" class="btn color-white bg-secondary rounded-0">vai alla pagina</a>
                    </div>
                </div>
                <div class="col-12 col-sm-4 mb-3 py-4 text-center">
                    <div class="py-5 h-100 box-hp box-amministrazione-trasparente">
                        <h2>Cooperativa servizi</h2>
                        <p>Ente strumentale all'Ordine</p>
                        <a href="/cooperativa-servizi/" title="Cooperativa servizi Dottori Commercialisti A RL" class="btn color-white bg-secondary rounded-0">vai alla pagina</a>
                    </div>
                </div>
                <div class="col-12 col-sm-4 mb-3 py-4 text-center">
                    <div class="py-5 h-100 box-hp box-hp-formazione">
                        <h2>Formazione</h2>
                        <p>Consulta le offerte formative</p>
                        <a href="/formazione/" title="Consulta le offerte formative dekll'Ordine" class="btn color-white bg-secondary rounded-0">vai alla pagina</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid bg-grey color-default">
        <div class="container px-0">
            <div class="row py-5">
                <div class="col text-center">
                    <?php get_template_part('template-parts/banners/banners', 'hp'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-corporate color-default m-0 p-0">
        <div class="container py-5">
            <div class="row row-eq-height color-white">
                <div class="col-12 text-center mb-3 color-white">
                    <?php if (is_active_sidebar('hp-sportello-0')) : ?>
                        <?php dynamic_sidebar('hp-sportello-0'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-sm-3 my-3 text-center">
                    <?php if (is_active_sidebar('hp-sportello-1')) : ?>
                        <?php dynamic_sidebar('hp-sportello-1'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-sm-3 my-3 text-center">
                    <?php if (is_active_sidebar('hp-sportello-2')) : ?>
                        <?php dynamic_sidebar('hp-sportello-2'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-sm-3 my-3 text-center">
                    <?php if (is_active_sidebar('hp-sportello-3')) : ?>
                        <?php dynamic_sidebar('hp-sportello-3'); ?>
                    <?php endif; ?>
                </div>
                <div class="col-12 col-sm-3 my-3 text-center">
                    <?php if (is_active_sidebar('hp-sportello-4')) : ?>
                        <?php dynamic_sidebar('hp-sportello-4'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-white color-default m-0 p-0">
        <div class="container">
            <div class="row row-eq-height py-5">
                <div class="col-12 col-sm-2 mb-3 py-4 text-center offset-sm-1">
                    <a href="http://www.cndcec.it/" target="_blank" title="">
                        <img src="http://www.odctrento.it/wp-content/uploads/2016/10/image002.jpg" class="img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-12 col-sm-2 mb-3 py-4 text-center">
                    <a href="http://www.commercialistideltriveneto.com/" target="_blank" title="">
                        <img src="http://www.odctrento.it/wp-content/uploads/2016/10/image003.jpg" class="img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-12 col-sm-2 mb-3 py-4 text-center">
                    <a href="http://www.cnpadc.it/" target="_blank" title="">
                        <img src="http://www.odctrento.it/wp-content/uploads/2016/10/image004.jpg" class="img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-12 col-sm-2 mb-3 py-4 text-center">
                    <a href="http://www.cassaragionieri.it/" target="_blank" title="">
                        <img src="http://www.odctrento.it/wp-content/uploads/2016/10/image005.jpg" class="img-fluid" alt="" />
                    </a>
                </div>
                <div class="col-12 col-sm-2 mb-3 py-4 text-center">
                    <a href="http://www.fondazionenazionalecommercialisti.it/" target="_blank" title="">
                        <img src="http://www.odctrento.it/wp-content/uploads/2016/10/image006.png" class="img-fluid" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

</main>

<?php get_template_part('template-parts/footer') ?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/js/modernizr.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/js/main.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>

<script>
    $(document).ready(function () {
        $('body').on('mouseenter mouseleave', '.dropdown', function (e) {
            var _d = $(e.target).closest('.dropdown');
            _d.addClass('show');
            setTimeout(function () {
                _d[_d.is(':hover') ? 'addClass' : 'removeClass']('show');
            }, 10);
        });

        <?php if ($posts_hp->post_count > 2) { ?>
        $('.slider').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            vertical: true,
            arrows: false,
            dots: true
        });
        <?php } ?>

        $('.banners-slider').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            arrows: false
        });
    });
</script>

</body>
</html>