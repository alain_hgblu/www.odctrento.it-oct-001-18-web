<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 08/03/2018
 * Time: 14:42
 */

# /wp-content/themes/odctrento2018/utility/import_iscritti.php

define('WP_USE_THEMES', false);
require('../../../../wp-blog-header.php');

$csv_map_data = [
    '?iscritti_id' => 0,
    '?iscritti_idcollegio' => 1,
    'iscritti_collegio' => 2,
    'iscritti_tipologia_iscrizione' => 3,
    'iscritti_qualifica' => 4,
    'iscritti_cognome' => 5,
    'iscritti_nome' => 6,
    'iscritti_indirizzo' => 7,
    'iscritti_citta' => 8,
    'iscritti_provincia' => 9,
    'iscritti_cap' => 10,
    'iscritti_telefono' => 11,
    'iscritti_fax' => 12,
    'iscritti_email' => 13,
    'iscritti_data' => 14,
    'iscritti_numero' => 15,
    'iscritti_codice_fiscale' => 16,
    'iscritti_codcard' => 17,
    'iscritti_attivo' => 18,
    'iscritti_password' => 19,
    'iscritti_provenienza' => 20,
    'iscritti_tipoiscrizione' => 21,
    'iscritti_nome_studio' => 22,
    'iscritti_note_iscritto' => 23,
    'iscritti_revisore_contabile' => 24,
    'iscritti_ctg' => 25,
    'iscritti_data_di_nascita_gg' => 26,
    'iscritti_data_di_nascita_mm' => 27,
    'iscritti_data_di_nascita_aa' => 28,
    'iscritti_luogo_di_nascita' => 29,
    'iscritti_provincia_di_nascita' => 30,
    'iscritti_cellulare' => 31,
    'iscritti_note' => 32,
    'iscritti_titolo_di_studi_abilitante' => 33,
    'iscritti_ulteriore_titolo_di_studi' => 34,
    'iscritti_pec' => 35,
    'iscritti_formazione_professionale_continua' => 36,
    'iscritti_sito' => 37,
    'iscritti_anzianita' => 38,
    'iscritti_email_albo' => 39,
    'iscritti_data_eliminazione' => 40,
    'iscritti_status' => 41,
    'iscritti_ruolo' => 42,
    'iscritti_rappresentante' => 43,
    'iscritti_soci' => 44,
    'iscritti_dati_revisore' => 45,
    'iscritti_pec_reginde' => 46,
];

# estrae lista iscritti già presenti nel database (crea array con KEY = NUMERO ISCRIZIONEID e VALUE = iscritto,
$iscritti_data = [];

$csv_file = 'iscritti_sezioneA.csv';

#estrae dati da file CSV
if (($handle = fopen($csv_file, "r")) !== FALSE) {
    $row = 0;
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
        $data = array_map("utf8_encode", $data);
        if ($row > 0) {
            $csv_data[] = $data;
        }
        $row++;
    }
    fclose($handle);
}

function checkBoolean($val) {
    if($val <= 0) {
        return 0;
    } else {
        return $val;
    }
}

function checkString($val) {
    if(trim($val) == 'NULL' || trim($val) == '-') {
        $val = '';
    }
    return trim($val);
}
//print_r($csv_data);

$i = 0;
# elaboro lista completa dati da file CSV
foreach ($csv_data as $Dk => $Dv) {

    $array_iscritto = [
//        'ID' => 73,
        'post_title' => $Dv[$csv_map_data['iscritti_cognome']] . ' ' . $Dv[$csv_map_data['iscritti_nome']],
        'post_type' => 'alboiscritti',
        'post_status' => 'publish'

    ];

    $array_iscritti_custom_fields = [
        'iscritti_collegio' => checkString($Dv[$csv_map_data['iscritti_collegio']]),
        'iscritti_tipologia_iscrizione' => checkString($Dv[$csv_map_data['iscritti_tipologia_iscrizione']]),
        'iscritti_qualifica' => checkString($Dv[$csv_map_data['iscritti_qualifica']]),
        'iscritti_cognome' => checkString($Dv[$csv_map_data['iscritti_cognome']]),
        'iscritti_nome' => checkString($Dv[$csv_map_data['iscritti_nome']]),
        'iscritti_indirizzo' => checkString($Dv[$csv_map_data['iscritti_indirizzo']]),
        'iscritti_citta' => checkString($Dv[$csv_map_data['iscritti_citta']]),
        'iscritti_provincia' => checkString($Dv[$csv_map_data['iscritti_provincia']]),
        'iscritti_cap' => checkString($Dv[$csv_map_data['iscritti_cap']]),
        'iscritti_telefono' => checkString($Dv[$csv_map_data['iscritti_telefono']]),
        'iscritti_fax' => checkString($Dv[$csv_map_data['iscritti_fax']]),
        'iscritti_email' => checkString($Dv[$csv_map_data['iscritti_email']]),
        'iscritti_data' => checkString($Dv[$csv_map_data['iscritti_data']]),
        'iscritti_numero' => checkString(intval($Dv[$csv_map_data['iscritti_numero']])),
        'iscritti_codice_fiscale' => checkString($Dv[$csv_map_data['iscritti_codice_fiscale']]),
        'iscritti_codcard' => checkString($Dv[$csv_map_data['iscritti_codcard']]),
        'iscritti_attivo' => checkString($Dv[$csv_map_data['iscritti_attivo']]),
        'iscritti_password' => checkString($Dv[$csv_map_data['iscritti_password']]),
        'iscritti_provenienza' => checkString($Dv[$csv_map_data['iscritti_provenienza']]),
        'iscritti_tipoiscrizione' => checkString($Dv[$csv_map_data['iscritti_tipoiscrizione']]),
        'iscritti_nome_studio' => checkString($Dv[$csv_map_data['iscritti_nome_studio']]),
        'iscritti_note_iscritto' => checkString($Dv[$csv_map_data['iscritti_note_iscritto']]),
        'iscritti_revisore_contabile' => checkBoolean($Dv[$csv_map_data['iscritti_revisore_contabile']]),
        'iscritti_ctg' => $Dv[$csv_map_data['iscritti_ctg']],
        'iscritti_data_di_nascita' => str_pad($Dv[$csv_map_data['iscritti_data_di_nascita_gg']], 2, '0', STR_PAD_LEFT) . '/' . str_pad($Dv[$csv_map_data['iscritti_data_di_nascita_mm']], 2, '0', STR_PAD_LEFT) . '/' . $Dv[$csv_map_data['iscritti_data_di_nascita_aa']] ,
        'iscritti_luogo_di_nascita' => $Dv[$csv_map_data['iscritti_luogo_di_nascita']],
        'iscritti_provincia_di_nascita' => checkString($Dv[$csv_map_data['iscritti_provincia_di_nascita']]),
        'iscritti_cellulare' => checkString($Dv[$csv_map_data['iscritti_cellulare']]),
        'iscritti_note' => checkString($Dv[$csv_map_data['iscritti_note']]),
        'iscritti_titolo_di_studi_abilitante' => checkString($Dv[$csv_map_data['iscritti_titolo_di_studi_abilitante']]),
        'iscritti_ulteriore_titolo_di_studi' => checkString($Dv[$csv_map_data['iscritti_ulteriore_titolo_di_studi']]),
        'iscritti_pec' => checkString($Dv[$csv_map_data['iscritti_pec']]),
        'iscritti_formazione_professionale_continua' => checkString($Dv[$csv_map_data['iscritti_formazione_professionale_continua']]),
        'iscritti_sito' => checkString($Dv[$csv_map_data['iscritti_sito']]),
        'iscritti_anzianita' => checkString($Dv[$csv_map_data['iscritti_anzianita']]),
        'iscritti_email_albo' => checkString($Dv[$csv_map_data['iscritti_email_albo']]),
        'iscritti_data_eliminazione' => checkString($Dv[$csv_map_data['iscritti_data_eliminazione']]),
        'iscritti_status' => checkString($Dv[$csv_map_data['iscritti_status']]),
        'iscritti_ruolo' => checkString($Dv[$csv_map_data['iscritti_ruolo']]),
        'iscritti_rappresentante' => checkString($Dv[$csv_map_data['iscritti_rappresentante']]),
        'iscritti_soci' => checkString($Dv[$csv_map_data['iscritti_soci']]),
        'iscritti_dati_revisore' => checkString($Dv[$csv_map_data['iscritti_dati_revisore']]),
        'iscritti_pec_reginde' => checkString($Dv[$csv_map_data['iscritti_pec_reginde']])
    ];
//    echo "<pre>"; print_r($array_iscritti_custom_fields); echo "</pre>";
//    exit;

# inserisce / aggiorna iscritto
    $post_id = wp_insert_post($array_iscritto);

# imposto ID per aggiornamento custom fields (controllo se nuovo iscritto o aggiornamento esistente)
//    $post_id = $array_iscritto['ID'];


# inserisce / aggiorna campi custom fields
    foreach ($array_iscritti_custom_fields AS $Tk => $Tv) {
        update_field($Tk, $Tv, $post_id);
    }

    $i++;
    if($i >= 5) {
        exit;
    }
//    exit;
}
print_r($array_iscritti_custom_fields);

