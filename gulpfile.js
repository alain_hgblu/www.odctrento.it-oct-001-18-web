// GULP
var NODE_PATH = 'C:\\Users\\Alain\\AppData\\Roaming\\npm\\node_modules\\';
var gulp = require('gulp');


// PHP CONNECT
var php = require(NODE_PATH + 'gulp-connect-php');
gulp.task('php', function() {

    php.server({ base: '.', port: 8010, keepalive: true});
});

// BROWSER SYNC
var browserSync = require(NODE_PATH + 'browser-sync');
var reload  = browserSync.reload;
gulp.task('browser-sync',['styles', 'php'], function() {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
});


// IMMAGINI
var imagemin = require(NODE_PATH + 'gulp-imagemin');

gulp.task('images', function() {
    return gulp.src('src/images/*.*')
        .pipe(imagemin())
        .pipe(gulp.dest('assets/images/'));
});

// SCRIPT JS
var uglify = require(NODE_PATH + 'gulp-uglify');
var concat = require(NODE_PATH + 'gulp-concat');
var stripDebug = require(NODE_PATH + 'gulp-strip-debug');
var size = require(NODE_PATH + 'gulp-size');

gulp.task('scripts', function() {
    gulp.src('src/js/*.js')
        .pipe(stripDebug())
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(size())
        .pipe(gulp.dest('assets/js/'))
        .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation;
});

// STILI CSS
var csso = require(NODE_PATH + 'gulp-csso');
var autoprefix = require(NODE_PATH + 'gulp-autoprefixer');

gulp.task('styles', function() {
    gulp.src('src/css/*.{scss,css}')
        .pipe(autoprefix())
        .pipe(concat('main.min.css'))
        .pipe(csso())
        .pipe(size())
        .pipe(gulp.dest('assets/css/'))
        .pipe(browserSync.reload({stream: true})); // prompts a reload after compilation;
});


// WATCHERS
gulp.task('watch', function() {
    gulp.watch('src/css/*.scss', ['styles']);
    gulp.watch('src/images/*.*', ['images']);
    gulp.watch('src/js/*.js', ['scripts']);
    gulp.watch(['./*.php'], [reload]);
});


// DEFAULT
gulp.task('default', [ 'browser-sync', 'watch' ]);