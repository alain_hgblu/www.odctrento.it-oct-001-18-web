<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage ODC Trento 2018
 * @since ODC Trento 2018
 */
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ordine dei Commercialisti e degli Esperti Contabili di Trento e Rovereto</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Encode+Sans:300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet"
          href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/css/style.css">
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/assets/css/main.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
</head>
<body class="interna archive">

<?php get_template_part('template-parts/header') ?>

<main class="cd-main-content maincontent">
    <div class="container-fluid bg-white color-default">
        <div class="container px-0">
            <div class="d-none d-lg-block col-lg-9 offset-lg-3">
                <nav aria-label="breadcrumb">
                    <?php echo the_breadcrumb() ?>
                </nav>
            </div>
            <div class="row py-3">
                <div class="d-none d-lg-block col-lg-3">
                    <div class="sectionmenu my-3">
                        <nav class="nav flex-column">
                            <?php get_template_part('template-parts/menus/sezione') ?>
                            <?php if(get_post_type() == 'alboiscritti') get_template_part('template-parts/albo-iscritti/iscritti-form'); ?>
                        </nav>
                    </div>
                </div>
                <div class="col col-lg-9">
                    <?php
                    $post_type = get_post_type();
                    switch ($post_type) {
                        case "alboiscritti":
                            get_template_part('template-parts/albo-iscritti/iscritti');
                            break;
                        case "ammtrasparente":
                            get_template_part('template-parts/amministrazione-trasparente/lista');
                            break;
                        case "appuntamenti":
                            get_template_part('template-parts/eventi/eventi');
                            break;
                        default:
                            get_template_part('template-parts/news/news');
                            break;
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_template_part('template-parts/footer') ?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/js/modernizr.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/mega-site-navigation/js/main.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/it.js"></script>

<script>
    $(document).ready(function () {
        $('body').on('mouseenter mouseleave', '.dropdown', function (e) {
            var _d = $(e.target).closest('.dropdown');
            _d.addClass('show');
            setTimeout(function () {
                _d[_d.is(':hover') ? 'addClass' : 'removeClass']('show');
            }, 10);
        });

        <?php if($post_type == 'alboiscritti') { ?>

        $('#iscritti_citta').select2();
        $('#iscritti_tipologia_iscrizione').select2({minimumResultsForSearch: -1});

        $('.item-iscritto h3').on('click', function () {
            $(this).parent().find('.info').toggle();
        });
        <?php } ?>

        $('#accordion-iscritti').on('shown.bs.collapse', function () {
            $(this).find('h5 > .svg-inline--fa').attr('data-icon', 'angle-right');
            clicked = $(this).find('.collapse.show');
            clicked.parent().find('h5 > .svg-inline--fa').attr('data-icon', 'angle-down');
        });
    });
</script>
</body>
</html>