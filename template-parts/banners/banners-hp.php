<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
$args = array(
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'banners',
    'post_status' => 'publish',
    'posts_per_page' => 3
);
global $banners_hp;
$banners_hp = new WP_Query($args);
?>
<div class="banners-slider">
    <?php if ($banners_hp->have_posts()) { ?>
        <?php foreach ($banners_hp->posts as $Tv) { ?>
            <div>
                <a href="<?php echo get_field('banners_url', $Tv->ID) ?>" title="" target="_blank">
                    <?php echo get_the_post_thumbnail($Tv->ID, '', array('class' => 'img-fluid', 'alt' => $Tv->post_title, 'style' => 'display: initial')); ?>
                </a>
            </div>
        <?php } ?>
    <?php } ?>
</div>
