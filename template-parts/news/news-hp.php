<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
$args = array(
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'news_homepage',
            'compare' => '=',
            'value' => '1'
        )
    )
);
global $posts_hp;
$posts_hp = new WP_Query($args);
?>
<h2 class="color-white">In evidenza</h2>
<div class="slider">
    <?php if ($posts_hp->have_posts()) { ?>
        <?php while ($posts_hp->have_posts()) {
            $posts_hp->the_post(); ?>
            <div class="news-item" style="height:140px; overflow-y: hidden">
                <span class="date bg-corporate color-white pl-1"><?php echo the_date() ?></span>
                <a href="<?php echo get_permalink() ?>" title="">
                    <h3 class="color-secondary">
                        <?php echo the_title() ?>
                    </h3>
                    <p class="color-white">
                        <?php echo get_the_excerpt(); ?>
                    </p>
                </a>
            </div>
        <?php } ?>
    <?php } ?>
</div>
