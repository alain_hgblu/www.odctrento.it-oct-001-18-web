<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
?>
<h1><?php echo single_cat_title() ?></h1>
<?php if (have_posts()) { ?>
    <?php while (have_posts()) {
        the_post(); ?>
        <div class="news-item">
            <span class="date bg-white color-secondary pl-1"><?php echo the_date() ?></span>
            <h4 class="color-secondary pt-4">
                <a href="<?php echo get_permalink() ?>" title=""><?php echo the_title() ?></a>
            </h4>
            <p class="color-black">
                <?php echo get_the_excerpt(); ?>
            </p>
        </div>
    <?php } ?>
<?php } ?>
