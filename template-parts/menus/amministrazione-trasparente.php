<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 13/03/2018
 * Time: 10:33
 */

echo $id_anchestor = end(get_post_ancestors());
$post_type = get_post_type();

if ($post_type == 'page') {
    switch ($id_anchestor) {
        case 92: // ordine
            $menu_name = 'menu-ordine';
            break;
        case 115: // albo
            $menu_name = 'menu-albo';
            break;
        case 127: // tirocinio - praticanti
            $menu_name = 'menu-praticanti';
            break;
        case 135: // formazione
            $menu_name = 'menu-formazione';
            break;
        case 96: // comunicazioni
            $menu_name = 'menu-comunicazioni';
            break;
        case 100: // modulistica
            $menu_name = 'menu-modulistica';
            break;
    }
}

if ($post_type == 'post') {
    $menu_name = 'menu-news';
}

if ($menu_name != '') {
    $array_menu = wp_get_nav_menu_items($menu_name);
}
?>
<?php foreach ($array_menu as $Mk => $Mv) { ?>
    <a href="<?php echo $Mv->url ?>" class="nav-link<?php if ($Mv->object_id == get_the_ID()) echo ' active' ?>"
       title=""><?php echo $Mv->title ?></a>
<?php } ?>
