<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 13/03/2018
 * Time: 10:33
 */

$categories = get_the_category();
$category_id = $categories[0]->cat_ID;

$id_anchestor = end(get_post_ancestors());
$post_type = get_post_type();

if ($post_type == 'page') {
    switch ($id_anchestor) {
        case 92: // ordine
            $menu_name = 'menu-ordine';
            break;
        case 115: // albo
            $menu_name = 'menu-albo';
            break;
        case 463: // tirocinio
            $menu_name = 'menu-tirocinio';
            break;
        case 135: // formazione
            $menu_name = 'menu-formazione';
            break;
        case 96: // comunicazioni
            $menu_name = 'menu-comunicazioni';
            break;
        case 484: // normativa
            $menu_name = 'menu-normativa';
            break;
        /*case 100: // cooperativa e servizi
            $menu_name = 'menu-cooperativa-servizi';
            break;*/
    }
}

if ($post_type == 'post' || $post_type == 'appuntamenti') {
    $menu_name = 'menu-news';
}

if ($menu_name != '') {
    $array_menu = wp_get_nav_menu_items($menu_name);
}
?>
<?php foreach ($array_menu as $Mk => $Mv) { ?>
    <a href="<?php echo $Mv->url ?>" class="nav-link<?php if($Mv->classes[0] != '') echo ' ' . $Mv->classes[0]; ?><?php if ($Mv->object_id == get_the_ID() || $category_id == $Mv->object_id) echo ' active' ?>"
       title="" target="<?php echo $Mv->target ?>"><?php echo $Mv->title; ?></a>
<?php } ?>


<?php
if ($post_type == 'ammtrasparente') {
    $args = array('post_type' => 'ammtrasparente', 'post_parent' => 0, 'orderby' => 'menu_order', 'order' => 'ASC', 'depth' => 1, 'posts_per_page' => 100);
    $amministrazione_query = new WP_Query($args);
    foreach ($amministrazione_query->posts as $Tk => $Tv) {
?>
        <a href="<?php echo get_the_permalink($Tv->ID) ?>" class="nav-link<?php if($Tv->classes[0] != '') echo ' ' . $Tv->classes[0]; ?><?php if ($Tv->ID == get_the_ID() || $Tv->ID == $id_anchestor) echo ' active' ?>"
           title=""><?php echo $Tv->post_title; ?></a>
        <?php
        $Tarray_menu[] = array('title' => $Tv->post_title, 'url' => get_the_permalink($Tv->ID));
    }
}
?>