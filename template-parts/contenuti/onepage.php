<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 12/03/2018
 * Time: 17:01
 */

$args = array('post_type'=>'page', 'post_parent' => get_the_ID(), 'orderby' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => 100);
$content_query = new WP_Query($args);
?>
<h1><?php the_title() ?></h1>
<div id="accordion" class="row">
    <?php if ($content_query->have_posts()) { ?>
        <?php while ($content_query->have_posts()) { $content_query->the_post(); $id_block = get_the_ID(); ?>
            <div class="col-12">
                <div class="card">
                    <h5 class="card-header mt-0" data-toggle="collapse" data-target="#block-<?php echo $id_block ?>" aria-expanded="true" aria-controls="block-<?php echo $id_block ?>"> <?php the_title() ?></h5>
                    <div id="block-<?php echo $id_block ?>" class="collapse show" data-parent="#accordion">
                        <div class="card-body">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
