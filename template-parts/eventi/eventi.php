<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
$args_appuntamenti = array(
    'orderby' => 'data_evento',
    'order' => 'ASC',
    'post_type' => 'appuntamenti',
    'post_status' => 'publish'
);

$title = 'Prossimi appuntamenti';

# archivio eventi
if (isset($_GET['archive'])) {
    $args_appuntamenti['meta_query'] = array(
        'relation' => 'AND',
        array(
            'key' => 'data_evento',
            'value' => date("Y-m-d"),
            'compare' => '<',
            'type' => 'DATE'
        )
    );
    $title = 'Archivio appuntamenti';

# prossimi eventi
} else {
    $args_appuntamenti['meta_query'] = array(
        'relation' => 'AND',
        array(
            'key' => 'data_evento',
            'compare' => '>=',
            'value' => date("Y-m-d"),
            'type' => 'DATE'
        )
    );
}

$months = array(
    '01' => 'gennaio',
    '02' => 'febbraio',
    '03' => 'marzo',
    '04' => 'aprile',
    '05' => 'maggio',
    '06' => 'giugno',
    '07' => 'luglio',
    '08' => 'agosto',
    '09' => 'settembre',
    '10' => 'ottobre',
    '11' => 'novembre',
    '12' => 'dicembre'
);
setlocale(LC_TIME, 'it');

$appuntamenti = new WP_Query($args_appuntamenti);
?>
<h1><?php echo $title ?></h1>
<div class="items-list">
    <?php if ($appuntamenti->have_posts()) { ?>
        <?php  while ($appuntamenti->have_posts()) { $appuntamenti->the_post(); ?>
            <div class="row item">
                <div class="col-4 col-sm-3 col-xl-2 text-center">
                    <div class="date pt-3 pb-2">
                        <span class="date-day py-1"><?php echo date('d', strtotime(get_field("data_evento", get_the_ID()))) ?></span>
                        <span class="date-month py-2"><?php echo $months[date('m', strtotime(get_field("data_evento", get_the_ID())))]; ?></span>
                    </div>
                </div>
                <div class="col-8 col-sm-9 col-xl-10">
                    <h4 class="title"><a href="<?php echo get_permalink() ?>" title=""><?php the_title() ?></a></h4>
                    <?php the_excerpt(); ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
