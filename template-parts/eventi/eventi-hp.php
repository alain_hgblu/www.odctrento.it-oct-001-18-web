<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
$args = array(
    'orderby' => 'data_evento',
    'order' => 'ASC',
    'post_type' => 'appuntamenti',
    'post_status' => 'publish',
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'data_evento',
            'compare' => '>=',
            'value' => date("Y-m-d"),
            'type' => 'DATE'
        ),
        array(
            'relation' => 'AND',
            array(
                'key' => 'evento_homepage',
                'compare' => '=',
                'value' => '1'
            )
        )
    )
);

$months = array(
    '01' => 'gennaio',
    '02' => 'febbraio',
    '03' => 'marzo',
    '04' => 'aprile',
    '05' => 'maggio',
    '06' => 'giugno',
    '07' => 'luglio',
    '08' => 'agosto',
    '09' => 'settembre',
    '10' => 'ottobre',
    '11' => 'novembre',
    '12' => 'dicembre'
);
setlocale(LC_TIME, 'it');

$posts = new WP_Query($args);
//print_r($appuntamentiHP);
//print_r($posts);
?>
<h2 class="color-white mb-5">Prossimi appuntamenti</h2>

<?php if ($posts->have_posts()) { ?>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <?php $i = 0;
        while ($posts->have_posts()) { ?>
            <?php $posts->the_post(); ?>
            <li class="nav-item">
                <a class="nav-link bg-white<?php if($i == 0) echo ' active'; ?>" id="home-tab" data-toggle="tab" href="#event<?php echo get_the_ID() ?>"
                   role="tab" aria-controls="home" aria-selected="true">
                    <span class="date-day color-black"><?php echo date('d', strtotime(get_field("data_evento", get_the_ID()))) ?></span>
                    <span class="date-month color-black"><?php echo $months[date('m', strtotime(get_field("data_evento", get_the_ID())))]; ?></span>
                </a>
            </li>
            <?php $i++;
        } ?>
    </ul>
<?php } ?>

<?php if ($posts->have_posts()) { ?>
    <div class="tab-content" id="myTabContent">
        <?php $i = 0; while ($posts->have_posts()) { ?>
            <?php $posts->the_post(); $url = get_field("evento_link_esterno", get_the_ID()); $url_target = '_blank'; if(trim($url) == '') { $url = get_permalink(); $url_target = ''; } ?>
            <div class="tab-pane fade show p-4<?php if($i == 0) echo ' active'; ?>" id="event<?php echo get_the_ID() ?>" role="tabpanel"
                 aria-labelledby="home-tab">
                <a href="<?php echo $url ?>" target="<?php echo $url_target ?>" title="<?php echo the_title() ?>">
                    <h3 class="color-corporate mt-0">
                        <?php echo the_title(); ?>
                    </h3>
                    <?php the_excerpt(); ?>
                </a>
            </div>
            <?php $i++;
        } ?>
    </div>
<?php } ?>
<div class="mt-4 ml-3">
    <a href="/appuntamenti/" class="color-white more-events-lnk" title="Vedi tutti gli appuntamenti">Vedi tutti gli appuntamenti</a>
</div>

<?php /* <div class="appuntamenti-item">
            <span class="date"><?php echo get_field("data_evento", get_the_ID()) ?></span>
            <h3><?php echo the_title() ?></h3>
            <p>
                <?php //echo get_field("sottotitolo", get_the_ID()); ?>
            </p>
        </div>



    <?php } ?>
<?php } ?>

<?php /*

<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active p-4" id="event1" role="tabpanel" aria-labelledby="home-tab">
        <h3 class="color-corporate mt-0">Appuntamento 1</h3>
        <p>
            Con la risoluzione n. 1/E del 5 gennaio 2018 l'Agenzia delle Entrate ha istituito la causale contributo 'ENAB' per la riscossione, tramite modello F24, dei contributi da destinare al finanziamento dell'Ente Nazionale Autonomo Bilaterale 'E.N.A.B. IMPRESE'.
        </p>
    </div>
    <div class="tab-pane fade p-4" id="event2" role="tabpanel" aria-labelledby="profile-tab">
        <h3 class="color-corporate mt-0">Appuntamento 2</h3>
        <p>
            Con la risoluzione n. 1/E del 5 gennaio 2018 l'Agenzia delle Entrate ha istituito la causale contributo 'ENAB' per la riscossione, tramite modello F24, dei contributi da destinare al finanziamento dell'Ente Nazionale Autonomo Bilaterale 'E.N.A.B. IMPRESE'.
        </p>
    </div>
    <div class="tab-pane fade p-4" id="event3" role="tabpanel" aria-labelledby="contact-tab">
        <h3 class="color-corporate mt-0">Appuntamento 3</h3>
        <p>
            Con la risoluzione n. 1/E del 5 gennaio 2018 l'Agenzia delle Entrate ha istituito la causale contributo 'ENAB' per la riscossione, tramite modello F24, dei contributi da destinare al finanziamento dell'Ente Nazionale Autonomo Bilaterale 'E.N.A.B. IMPRESE'.
        </p>
    </div>
</div>
 */ ?>
