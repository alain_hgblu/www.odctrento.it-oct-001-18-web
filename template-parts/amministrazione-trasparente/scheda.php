<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 12/03/2018
 * Time: 14:57
 */

$id_current_page = get_the_ID();
$args = array('post_type' => 'ammtrasparente', 'orderby' => 'menu_order', 'post_parent' => $id_current_page, 'order' => 'ASC');
$amministrazione_query = new WP_Query($args);
$count = $amministrazione_query->post_count;

# Pagina precedente
$id_parent_page = wp_get_post_parent_id($id_current_page);
$parent_post = get_post($id_parent_page);
?>
<h1>Amministrazione trasparente</h1>
<?php if($id_parent_page > 0) { ?>
<div class="mb-3 px-2" style="border:1px solid #cccccc; background-color: #f4f4f4">
    <small>Torna a <em><a href="<?php echo the_permalink($id_parent_page) ?>" title=""><?php echo $parent_post->post_title ?></a></em></small>
</div>
<?php } ?>
<h2><?php echo the_title() ?></h2>
<?php the_post(); ?>
<?php if($count > 0) { ?>
    <ul>
        <?php foreach ($amministrazione_query->posts as $Tv) { ?>
        <li>
            <a href="<?php echo the_permalink($Tv->ID) ?>" title="<?php echo $Tv->post_title; ?>"><?php echo $Tv->post_title; ?></a>
        </li>
    <?php } ?>
<?php } else { ?>
    <?php the_content(); ?>
<?php } ?>
