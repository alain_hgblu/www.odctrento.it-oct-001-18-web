<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 12/03/2018
 * Time: 14:57
 */

$args = array('post_type' => 'ammtrasparente', 'post_parent' => 0, 'orderby' => 'menu_order', 'order' => 'ASC', 'depth' => 1, 'posts_per_page' => 100);
$amministrazione_query = new WP_Query($args);
?>
<h1>Amministrazione trasparente</h1>
<div class="row">
    <?php foreach ($amministrazione_query->posts as $Tv) { ?>
        <div class="col-12">
            <div class="card">
                <h5 class="card-header mt-0">
                    <a href="<?php echo the_permalink($Tv->ID) ?>" title=""><?php echo $Tv->post_title; ?></a>
                </h5>
                <div class="card-body">
                    <ul>
                        <?php
                        $args2 = array('post_type' => 'ammtrasparente', 'post_parent' => $Tv->ID, 'orderby' => 'menu_order', 'order' => 'ASC', 'depth' => 1, 'posts_per_page' => 100);
                        $amministrazione_query2 = new WP_Query($args2);
                        foreach ($amministrazione_query2->posts as $Tv2) { ?>
                            <li>
                                <a href="<?php echo the_permalink($Tv2->ID) ?>" title=""><?php echo $Tv2->post_title; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

<?php /*
 $args = array('post_type' => 'ammtrasparente', 'orderby' => 'menu_order', 'order' => 'ASC', 'title_li' => 'Amministrazione trasparente');
 wp_list_pages($args) */ ?>
