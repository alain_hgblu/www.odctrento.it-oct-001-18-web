<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 12/03/2018
 * Time: 14:57
 */
$id_anchestor = end(get_post_ancestors());

$args = array('post_type' => 'ammtrasparente', 'post_parent' => 0, 'orderby' => 'menu_order', 'order' => 'ASC', 'depth' => 2, 'posts_per_page' => 140);
$amministrazione_query = new WP_Query($args);
?>
<h1>Amministrazione trasparente *</h1>
<div class="row">
    <ul>
        <?php
        $args = array('post_type' => 'ammtrasparente', 'post_parent' => 0, 'orderby' => 'menu_order', 'order' => 'ASC', 'depth' => 1, 'posts_per_page' => 100);
        $amministrazione_query = new WP_Query($args);
        foreach ($amministrazione_query->posts as $Tv) {
            if (!$Tv->post_parent) { ?>
                <li>
                    <a href="<?php echo the_permalink($Tv->ID) ?>" title=""><?php echo $Tv->post_title; ?></a>
                </li>
            <?php }
        } ?>
    </ul>
</div>

<?php /*
 $args = array('post_type' => 'ammtrasparente', 'orderby' => 'menu_order', 'order' => 'ASC', 'title_li' => 'Amministrazione trasparente');
 wp_list_pages($args) */ ?>
