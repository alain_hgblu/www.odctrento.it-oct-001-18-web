<?php
$id_anchestor = end(get_post_ancestors());
switch ($id_anchestor) {
    case 92: // ordine
        $current_item_menu = 78;
        break;
    case 52: // amministrazione trasparente
        $current_item_menu = 78;
        break;
    case 115: // albo
        $current_item_menu = 85;
        break;
    case 463: // tirocinio
        $current_item_menu = 86;
        break;
    case 135: // formazione
        $current_item_menu = 87;
        break;
    case 87: // comunicazione
        $current_item_menu = 88;
        break;
    case 100: // modulistica
        $current_item_menu = 89;
        break;
    case 19: // contatti
        $current_item_menu = 19;
        break;
}

if (get_post_type() == 'post') {
    $current_item_menu = 88; // comunicazioni
}

if (get_the_ID() == 19) {
    $current_item_menu = 19; // contatti
}

function wp_get_menu_array($current_menu)
{
    $array_menu = wp_get_nav_menu_items($current_menu);
//    print_r($array_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID'] = $m->ID;
            $menu[$m->ID]['title'] = $m->title;
            $menu[$m->ID]['url'] = $m->url;
            $menu[$m->ID]['children'] = array();
            $menu[$m->ID]['postID'] = $m->object_id;
            $menu[$m->ID]['classes'] = $m->classes[0];
            $menu[$m->ID]['target'] = $m->target;
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID'] = $m->ID;
            $submenu[$m->ID]['title'] = $m->title;
            $submenu[$m->ID]['url'] = $m->url;
            $submenu[$m->ID]['postID'] = $m->object_id;
            $submenu[$m->ID]['classes'] = $m->classes[0];
            $submenu[$m->ID]['target'] = $m->target;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
}


$a = wp_get_menu_array('mainmenu');
?>
<header class="container-fluid cd-main-header">

    <ul class="cd-header-buttons">
        <!--        <li><a class="cd-search-trigger" href="#cd-search">Search<span></span></a></li>-->
        <li><a class="cd-nav-trigger" href="#cd-primary-nav"><span></span></a></li>
    </ul> <!-- cd-header-buttons -->

    <div class="container search-container">
        <div class="logo">
            <a href="/" title="">
                <img src="/wp-content/themes/odctrento2018/assets/images/logo-odctrento.svg" alt=""/>
            </a>
        </div>
        <div class="text-right">
            <form class="bop-nav-search" role="search" method="get" action="/albo-iscritti/">
                <div>
                    <label class="screen-reader-text" for="s">Ricerca iscritti</label>
                    <input type="text" value="" name="search" id="s">
                    <button class="top-search bg-corporate"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light color-default mainmenu" role="navigation">

        <div class="container">
            <div id="navbarNav" class="navbar-collapse collapse">
                <ul class="nav navbar-nav ml-auto">
                    <?php foreach ($a as $mi => $m) { ?>
                        <li class="nav-item<?php if (count($m['children']) > 0) { ?> dropdown<?php } ?><?php if ($m['ID'] == $current_item_menu || $m['postID'] == $current_item_menu) { ?> active<?php } ?>">
                            <a class="nav-link px-2 text-uppercase <?php if (count($m['children']) > 0) { ?> dropdown-toggle<?php } ?> hgactive1"
                               href="<?php echo $m['url'] ?>" <?php if (count($m['children']) > 0) { ?> id="navbarDropdown" data-toggle="dropdown"<?php } ?>
                               role="button" aria-haspopup="true" aria-expanded="false">
                                <?php echo $m['title']; ?>
                            </a>
                            <?php if (count($m['children']) > 0) { ?>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php foreach ($m['children'] as $c) { ?>
                                        <a class="dropdown-item<?php if($c['classes'] != '') echo ' ' . $c['classes'] ?>"
                                           href="<?php echo $c['url'] ?>" title="" target="<?php echo $c['target'] ?>"><?php echo $c['title'] ?></a>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <!--        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">-->
        <!--            <span class="navbar-toggler-icon"></span>-->
        <!--        </button>-->
    </nav>

    <nav class="cd-nav">
        <ul id="cd-primary-nav" class="cd-primary-nav is-fixed">
            <?php foreach ($a as $mi => $m) { ?>
                <li class="nav-item<?php if (count($m['children']) > 0) { ?> has-children<?php } ?><?php if ($m['ID'] == $current_item_menu || $m['postID'] == $current_item_menu) { ?> active<?php } ?>">
                    <a class="nav-link px-2 text-uppercase <?php if (count($m['children']) > 0) { ?> dropdown-toggle<?php } ?> hgactive1"
                       href="<?php echo $m['url'] ?>" <?php if (count($m['children']) > 0) { ?> id="navbarDropdown" data-toggle="dropdown"<?php } ?>
                       role="button" aria-haspopup="true" aria-expanded="false">
                        <?php echo $m['title']; ?>
                    </a>
                    <?php if (count($m['children']) > 0) { ?>
                        <ul class="cd-secondary-nav is-hidden" aria-labelledby="navbarDropdown">
                            <li class="go-back"><a href="#0">Menu</a></li>
                            <?php foreach ($m['children'] as $c) { ?>
                                <li>
                                    <a class="dropdown-item<?php if($c['classes'] != '') echo ' ' . $c['classes'] ?>"
                                       href="<?php echo $c['url'] ?>" title="" target="<?php echo $c['target'] ?>"><?php echo $c['title'] ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul> <!-- primary-nav -->
    </nav> <!-- cd-nav -->
</header>
