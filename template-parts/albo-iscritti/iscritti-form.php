<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 27/03/2018
 * Time: 13:51
 */
# city list (iscritti_citta)
global $wpdb;
$query = "SELECT DISTINCT(meta_value) FROM {$wpdb->postmeta} WHERE meta_key = 'iscritti_citta' GROUP BY meta_value ORDER BY meta_value ASC";
$lista_citta = $wpdb->get_results( $query );
?>
<form id="ricerca-iscritti" class="m-2" name="ricerca-iscritti" method="post" action="/albo-iscritti/">
    <div class="form-group">
        <label for="search">Nome</label>
        <input id="search" class="form-control" name="search" value="<?php if(isset($_POST['search']) && trim($_POST['search']) != '') echo trim($_POST['search']); ?>" title="" placeholder="ricerca">
    </div>
    <div class="form-group">
        <label for="iscritti_citta">Comune</label>
        <select id="iscritti_citta" class="form-control selectpicker" name="iscritti_citta" title="Comune" data-live-search="true">
            <option value="">Tutti</option>
            <?php foreach( $lista_citta as $citta ) : ?>
                <option value="<?php echo trim($citta->meta_value) ?>"<?php if(isset($_POST['iscritti_citta']) && $_POST['iscritti_citta'] == trim($citta->meta_value)) echo ' selected' ?>><?php echo trim($citta->meta_value) ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="iscritti_tipologia_iscrizione">Tipologia</label>
        <select id="iscritti_tipologia_iscrizione" class="form-control" name="iscritti_tipologia_iscrizione" title="Tipologia">
            <option value="">Tutti</option>
            <option value="Sezione A"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Sezione A') echo ' selected' ?>>Sezione A</option>
            <option value="Sezione B"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Sezione B') echo ' selected' ?>>Sezione B</option>
            <option value="Praticanti"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Praticanti') echo ' selected' ?>>Praticanti</option>
            <option value="Elenco speciale"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Elenco speciale') echo ' selected' ?>>Elenco speciale</option>
            <option value="Società tra professionisti"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Società tra professionisti') echo ' selected' ?>>Società tra professionisti</option>
            <option value="Studi Associati"<?php if(isset($_POST['iscritti_tipologia_iscrizione']) && $_POST['iscritti_tipologia_iscrizione'] == 'Studi Associati') echo ' selected' ?>>Studi Associati</option>
        </select>
    </div>
    <div class="form-check">
        <input type="checkbox" id="iscritti_revisore_legale-1" class="form-check-input" name="iscritti_revisore_legale-1" value="1" title=""<?php if(isset($_POST['iscritti_revisore_legale-1'])) echo ' checked'; ?> />
        <label for="iscritti_revisore_legale-1">Revisore Legale</label>
    </div>
    <button type="submit" class="btn btn-primary mb-2">Cerca</button>
</form>
