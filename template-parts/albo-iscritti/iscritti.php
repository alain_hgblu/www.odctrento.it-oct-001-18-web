<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 01/03/2018
 * Time: 08:55
 */
$args = array(
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'alboiscritti',
    'post_status' => 'publish',
    'posts_per_page' => 30
);

$is_search = false;

# simple search
if (isset($_REQUEST['search']) && trim($_REQUEST['search']) != '') {
    $args['s'] = trim($_REQUEST['search']);
    $args['posts_per_page'] = 9999;
    $is_search = true;
}

#advanced search
$meta_query_acf = '';
if (isset($_GET['l']) && trim($_GET['l']) != '') {
    $meta_query_acf[] = array(
        'key' => 'iscritti_cognome',
        'compare' => 'REGEXP',
        'value' => '^' . trim($_GET['l'])
    );
}
if (isset($_POST['iscritti_citta']) && trim($_POST['iscritti_citta']) != '') {
    $meta_query_acf[] = array(
        'key' => 'iscritti_citta',
        'compare' => '=',
        'value' => trim($_POST['iscritti_citta'])
    );
}
if (isset($_POST['iscritti_tipologia_iscrizione']) && trim($_POST['iscritti_tipologia_iscrizione']) != '') {
    $meta_query_acf[] = array(
        'key' => 'iscritti_tipologia_iscrizione',
        'compare' => '=',
        'value' => trim($_POST['iscritti_tipologia_iscrizione'])
    );
}
if (isset($_POST['iscritti_revisore_legale-1'])) {
    $meta_query_acf[] = array(
        'key' => 'iscritti_revisore_contabile',
        'compare' => '=',
        'value' => intval(($_POST['iscritti_revisore_contabile']))
    );
}

if(count($meta_query_acf) > 0) {
    $args['meta_query'] = array(
        'relation' => 'AND',
        $meta_query_acf
    );
    $is_search = true;
}

$iscritti = new WP_Query($args);
?>
<h1>Elenco iscritti</h1>

<nav class="d-none d-sm-block">
    <ul class="pagination pagination-sm">
        <?php foreach (range('A', 'Z') as $char) { ?>
            <li class="page-item<?php if(trim($_GET['l']) == $char) echo ' active' ?>">
                <a href="?l=<?php echo $char ?>" class="page-link"><?php echo $char ?></a>
            </li>
        <?php } ?>
    </ul>
</nav>
<?php if ($is_search) { ?>
    <em>Trovati <strong><?php echo count($iscritti->posts) ?></strong> risultati</em>
<?php } ?>

<div id="accordion-iscritti" class="row">
    <?php if ($iscritti->have_posts()) { ?>
        <?php foreach ($iscritti->posts as $Tv) { $id_block = $Tv->ID; ?>
            <div class="col-12 item-iscritto">
                <div class="card">
                    <h5 class="card-header mt-0" data-toggle="collapse" data-target="#block-<?php echo $id_block ?>" aria-expanded="true" aria-controls="block-<?php echo $id_block ?>" style="position:relative"><?php echo $Tv->post_title; ?> <i class="fas fa-angle-right fa-lg" style="position:absolute;right:10px;"></i></h5>
                    <div id="block-<?php echo $id_block ?>" class="collapse hide" data-parent="#accordion-iscritti">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped mx-0 px-0">
                                    <tbody>
                                    <tr class="d-flex1">
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Tipologia:</strong> <?php echo get_field('field_5aa0e6b35189a', $Tv->ID); ?></td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Data iscrizione:</strong> <?php echo get_field('field_5aa0e728c4a01', $Tv->ID); ?></td>
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Num. iscrizione:</strong> <?php echo get_field('field_5aa0e7e8fc131', $Tv->ID); ?></td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Luogo di nascita:</strong> <?php echo get_field('field_5ac723d2dd093', $Tv->ID) ?> (<?php echo get_field('field_5ac726099132e', $Tv->ID) ?>)</td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Data di nascita:</strong> <?php echo get_field('field_5aa0e9919be13', $Tv->ID) ?></td>
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Codice fiscale:</strong> <?php echo get_field('field_5aa0e83dfc133', $Tv->ID) ?></td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Titolo di studio:</strong> <?php echo get_field('field_5aa0e77cc4a02', $Tv->ID) ?></td>
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Ulteriore titolo di studio:</strong> <?php echo get_field('field_5aa0e807fc132', $Tv->ID) ?></td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Revisore Legale:</strong> <?php echo get_field('field_5aa0ea3fe474d', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Stato:</strong> <?php echo get_field('field_5ac728cc471d2', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">Ruolo:</strong> <?php echo get_field('field_5ac7292e471d4', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">FPC:</strong> <?php echo get_field('field_5aa0e9bd9be15', $Tv->ID) ?></td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;"><i class="fas fa-address-card"></i></strong> <?php echo get_field('field_5aa0e8babaa95', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size: 11px; font-weight: 600;">CAP:</strong> <?php echo get_field('field_5aa0e90dbaa98', $Tv->ID) ?></td>
                                        <td class="py-2" colspan="2"><strong style="font-size: 11px; font-weight: 600;">Città:</strong> <?php echo get_field('field_5aa0e8cfbaa96', $Tv->ID) ?> (<?php echo get_field('field_5aa0e8f9baa97', $Tv->ID); ?>)</td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2"><strong style="font-size1: 11px; font-weight1: 600;"><i class="fas fa-phone"></i></strong> <?php echo get_field('field_5aa0e9ae9be14', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size1: 11px; font-weight1: 600;"><i class="fas fa-mobile-alt"></i></strong> <?php echo get_field('field_5ac725f469366', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size1: 11px; font-weight1: 600;"><i class="fas fa-fax"></i></strong> <?php echo get_field('field_5aa0ea059be16', $Tv->ID) ?></td>
                                        <td class="py-2"><strong style="font-size1: 11px; font-weight1: 600;"><i class="fas fa-envelope"></i></strong> <?php echo get_field('field_5aa0ea2ae474c', $Tv->ID) ?></td>
                                    </tr>
                                    <tr class="d-flex1">
                                        <td class="py-2" colspan="4"><strong style="font-size: 11px; font-weight: 600;">Note:</strong> <?php echo get_field('field_5aa145e3817e6', $Tv->ID) ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php /*<span><strong>Nato/a a</strong> <?php echo get_field('field_5ac723d2dd093', $Tv->ID) ?> (<?php echo get_field('field_5ac726099132e', $Tv->ID) ?>) il <?php echo get_field('field_5aa0e9919be13', $Tv->ID) ?></span><br/>
                            <span><strong>Codice fiscale:</strong> <?php echo get_field('field_5aa0e83dfc133', $Tv->ID); ?></span><br/>
                            <span><strong>Qualifica:</strong> <?php echo get_field('field_5aa0e61651899', $Tv->ID); ?></span><br/>
                            <span><strong>Numero iscrizione:</strong> <?php echo get_field('field_5aa0e7e8fc131', $Tv->ID); ?></span>
                            <span><strong>Data iscrizione:</strong> <?php echo get_field('field_5aa0e728c4a01', $Tv->ID); ?></span><br/>
                            <span><strong>Titolo di studio:</strong> <?php echo get_field('field_5aa0e77cc4a02', $Tv->ID); ?></span><br/>
                            <span><strong>Ulteriore titolo di studio:</strong> <?php echo get_field('field_5aa0e807fc132', $Tv->ID); ?></span><br/>
                            <span><strong>Revisore Legale:</strong> <?php if(get_field('field_5aa0ea3fe474d', $Tv->ID) == 1) echo 'SI'; else echo 'NO'; ?></span><br/>
                            <span><strong>Stato:</strong> <?php echo get_field('field_5ac728cc471d2', $Tv->ID); ?></span><br/>
                            <span><strong>Ruolo:</strong> <?php echo get_field('field_5ac7292e471d4', $Tv->ID); ?></span><br/>
                            <span><strong>Indirizzo:</strong> <?php echo get_field('field_5aa0e8babaa95', $Tv->ID); ?></span>
                            <span><strong>CAP:</strong> <?php echo get_field('field_5aa0e90dbaa98', $Tv->ID); ?></span>
                            <span><strong>Città:</strong> <?php echo get_field('field_5aa0e8cfbaa96', $Tv->ID); ?></span>
                            <span><strong>Provincia:</strong> <?php echo get_field('field_5aa0e8f9baa97', $Tv->ID); ?></span><br />
                            <span><strong>Telefono:</strong> <?php echo get_field('field_5aa0e9ae9be14', $Tv->ID); ?></span>
                            <span><strong>Cellulare:</strong> <?php echo get_field('field_5ac725f469366', $Tv->ID); ?></span>
                            <span><strong>Fax:</strong> <?php echo get_field('field_5aa0ea059be16', $Tv->ID); ?></span><br/>
                            <span><strong>E-mail:</strong> <?php echo get_field('field_5aa0ea2ae474c', $Tv->ID); ?></span><br/>
                            <span><strong>Formazione Professionale Continua:</strong> <?php echo get_field('field_5aa0e9bd9be15', $Tv->ID); ?></span><br/>
                            <span><strong>Note:</strong> <?php echo get_field('field_5aa145e3817e6', $Tv->ID); ?></span>*/ ?>
                        </div>
                    </div>
                </div>
                <?php /*<h3><?php echo $Tv->post_title; ?></h3>
                <!--                <em>--><?php //echo get_field('iscritti_nome_studio', $Tv->ID) ?><!--</em>-->
                <span><?php echo get_field('field_5aa0e6b35189a', $Tv->ID); //field_5aa0e61651899 ?></span>
                <div class="info" style="display: none">
                    <span><strong>Nato/a a</strong> <?php echo get_field('field_5ac723d2dd093', $Tv->ID) ?> (<?php echo get_field('field_5ac726099132e', $Tv->ID) ?>) il <?php echo get_field('field_5aa0e9919be13', $Tv->ID) ?></span><br/>
                    <span><strong>Codice fiscale:</strong> <?php echo get_field('field_5aa0e83dfc133', $Tv->ID); ?></span><br/>
                    <span><strong>Qualifica:</strong> <?php echo get_field('field_5aa0e61651899', $Tv->ID); ?></span><br/>
                    <span><strong>Numero iscrizione:</strong> <?php echo get_field('field_5aa0e7e8fc131', $Tv->ID); ?></span>
                    <span><strong>Data iscrizione:</strong> <?php echo get_field('field_5aa0e728c4a01', $Tv->ID); ?></span><br/>
                    <span><strong>Titolo di studio:</strong> <?php echo get_field('field_5aa0e77cc4a02', $Tv->ID); ?></span><br/>
                    <span><strong>Ulteriore titolo di studio:</strong> <?php echo get_field('field_5aa0e807fc132', $Tv->ID); ?></span><br/>
                    <span><strong>Revisore Legale:</strong> <?php if(get_field('field_5aa0ea3fe474d', $Tv->ID) == 1) echo 'SI'; else echo 'NO'; ?></span><br/>
                    <span><strong>Stato:</strong> <?php echo get_field('field_5ac728cc471d2', $Tv->ID); ?></span><br/>
                    <span><strong>Ruolo:</strong> <?php echo get_field('field_5ac7292e471d4', $Tv->ID); ?></span><br/>
                    <span><strong>Indirizzo:</strong> <?php echo get_field('field_5aa0e8babaa95', $Tv->ID); ?></span>
                    <span><strong>CAP:</strong> <?php echo get_field('field_5aa0e90dbaa98', $Tv->ID); ?></span>
                    <span><strong>Città:</strong> <?php echo get_field('field_5aa0e8cfbaa96', $Tv->ID); ?></span>
                    <span><strong>Provincia:</strong> <?php echo get_field('field_5aa0e8f9baa97', $Tv->ID); ?></span><br />
                    <span><strong>Telefono:</strong> <?php echo get_field('field_5aa0e9ae9be14', $Tv->ID); ?></span>
                    <span><strong>Cellulare:</strong> <?php echo get_field('field_5ac725f469366', $Tv->ID); ?></span>
                    <span><strong>Fax:</strong> <?php echo get_field('field_5aa0ea059be16', $Tv->ID); ?></span><br/>
                    <span><strong>E-mail:</strong> <?php echo get_field('field_5aa0ea2ae474c', $Tv->ID); ?></span><br/>
                    <span><strong>Formazione Professionale Continua:</strong> <?php echo get_field('field_5aa0e9bd9be15', $Tv->ID); ?></span><br/>
                    <span><strong>Note:</strong> <?php echo get_field('field_5aa145e3817e6', $Tv->ID); ?></span><br/>
                </div>*/ ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>
