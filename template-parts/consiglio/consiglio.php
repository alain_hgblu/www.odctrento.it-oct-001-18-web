<?php
/**
 * Created by PhpStorm.
 * User: alain
 * Date: 12/03/2018
 * Time: 17:01
 */
?>
<h1><?php the_title() ?></h1>
<div class="row">
    <?php
    $args = array('post_type'=>'page', 'post_parent' => get_the_ID(), 'orderby' => 'menu_order', 'order' => 'ASC', 'posts_per_page' => 100);
    $consiglio_query = new WP_Query($args); foreach($consiglio_query->posts as $Tv) {
        $img_default = '<img width="265" height="285" src="http://via.placeholder.com/265x285" class="card-img-top wp-post-image" alt="' . $Tv->post_title . '" />';
        $img = get_the_post_thumbnail($Tv->ID, '', array('class' => 'card-img-top img-fluid', 'alt' => $Tv->post_title));
        if($img == '') { $img = $img_default; }
        ?>
        <div class="col-12 col-sm-6 col-lg-4">
            <div class="card border-0">
                <?php echo $img ?>
                <div class="card-body px-0">
                    <h5 class="card-title"><?php echo $Tv->post_title; ?></h5>
                    <p class="card-text"><?php echo get_field("nominativo", $Tv->ID) ?></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
